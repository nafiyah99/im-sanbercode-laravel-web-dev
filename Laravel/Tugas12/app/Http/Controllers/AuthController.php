<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('regis');
    }

    public function main(Request $request)
    {
        //dd($request->all);
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('main', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
