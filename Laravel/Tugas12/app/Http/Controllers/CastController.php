<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'actor' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'actor.required' => "fill the name!",
            'age.required' => "fill the age!",
            'bio.required' => "fill the bio!"
        ]);

        DB::table('cast')->insert([
            'actor' => $request['actor'],
            'age' => $request['age'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('tampil', ['cast' => $cast]);
    }

    public function show($id){
        $castes = DB::table('cast')->find($id);

        return view('detail', ['castes' => $castes]);
    }

    public function edit($id){
        $castes = DB::table('cast')->find($id);

        return view('edit', ['castes' => $castes]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'actor' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'actor.required' => "fill the name!",
            'age.required' => "fill the age!",
            'bio.required' => "fill the bio!"
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'actor' => $request['actor'],
                    'age' => $request['age'],
                    'bio' => $request['bio']
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
