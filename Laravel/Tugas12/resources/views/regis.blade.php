@extends('layout.master')
@section('title')
    Halaman Pendaftaran
@endsection
@section('sub-title')
    Buat Account Baru! 
@endsection
@section('content')
        <h3>Sign Up Form</h3>

        <form action="/welcome" method="post">
            @csrf
            <label for="">First name:</label><br>
            <input type="text" name="fname"><br><br>
            <label for="">Last name:</label><br>
            <input type="text" name="lname"><br><br>
            <label for="">Gender:</label><br>
            <input type="radio" name="status" value="">Male<br>
            <input type="radio" name="status" value="">Female<br>
            <input type="radio" name="status" value="">Other<br><br>
            <label for="">Nationality:</label><br>
            <select name="country" id="">
                <option value="indonesian">Indonesian</option>
                <option value="other">Other</option>
            </select><br><br>
            <label for="">Language Spoken:</label><br>
            <input type="checkbox" name="language" id="">Bahasa Indonesia<br>
            <input type="checkbox" name="language" id="">English<br>
            <input type="checkbox" name="language" id="">Other<br><br>
            <label for="">Bio:</label><br>
            <textarea name="" id="" cols="20" rows="5"></textarea><br>

            <button type="submit" value="Sign Up">Sign Up</button>

        </form>
@endsection