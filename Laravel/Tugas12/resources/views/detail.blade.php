@extends('layout.master')
@section('title')
    Halaman Detail Pemeran
@endsection
@section('sub-title')
    List Pemeran
@endsection
@section('content')
    <h2>{{$castes->actor}}</h2>
    <h2>{{$castes->age}}</h2>
    <h2>{{$castes->bio}}</h2>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection