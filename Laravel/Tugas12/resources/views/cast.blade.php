@extends('layout.master')
@section('title')
    Halaman Pemeran
@endsection
@section('sub-title')
    List Pemeran
@endsection
@section('content')
    <form action="/cast" method="POST">
        @csrf
  <div class="form-group">
    <label>Actor's Name</label>
    <input type="text" name="actor" class="form-control">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Age</label>
    <input type="number" name="age" class="form-control">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection