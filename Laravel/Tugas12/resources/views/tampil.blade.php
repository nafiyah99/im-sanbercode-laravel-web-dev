@extends('layout.master')
@section('title')
    Halaman Tampil Pemeran
@endsection
@section('sub-title')
    List Pemeran
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Pemeran</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key => $item)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->actor}}</td>
        <td>
            <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>
    </tr>

    @empty
        <tr>
            <td>Data Pemeran Empty</td>
        </tr>
    @endforelse
  </tbody>
</table>
@endsection