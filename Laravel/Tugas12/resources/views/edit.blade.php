@extends('layout.master')
@section('title')
    Halaman Edit Pemeran
@endsection
@section('sub-title')
    List Pemeran
@endsection
@section('content')
    <form action="/cast/{{$castes->id}}" method="POST">
        @csrf
        @method('put')
  <div class="form-group">
    <label>Actor's Name</label>
    <input type="text" name="actor" value="{{$castes->actor}}" class="form-control">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Age</label>
    <input type="number" name="age" value="{{$castes->age}}" class="form-control">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$castes->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection