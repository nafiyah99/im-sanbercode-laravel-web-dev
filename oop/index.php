<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

// intansiasi objek

// release 0
$sheep = new Animal("shaun");
echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold-Blooded: " . $sheep->cold_blooded . "<br>"; // "no"
echo "<br>";

// release 1
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold-Blooded: " . $sungokong->cold_blooded . "<br>";
echo "Yell: " . $sungokong->yell() . "<br>";
echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold-Blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: " . $kodok->jump() . "<br>";
echo "<br>";

?>